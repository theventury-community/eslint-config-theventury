'use strict';

/* eslint-disable global-require */

const requireIndex = () => require('..');
const requireReact = () => require('../react');

describe('Can require', () => {
	test('base ruleset', () => {
		expect(requireIndex).not.toThrow();
	});

	test('react ruleset', () => {
		expect(requireReact).not.toThrow();
	});
});

const {ESLint} = require('eslint');
const eslintrc = require('../react');
const reactRules = require('../rules/react');

const eslint = new ESLint({
	useEslintrc: false,
	baseConfig: eslintrc,
});

/**
 * Lint a file text.
 *
 * @returns {Promise<Object>} the first linting result
 */
const lint = async (text) => {
	const lintResults = await eslint.lintText(text);

	return lintResults[0];
};

const wrapComponent = (body) => {
	return `import React from 'react';

export default class MyComponent extends React.Component {
${body}
}
`;
};

describe('React rules make sure that', () => {
	// suppress the react version console.error log
	console.error = jest.fn();

	test('the correct plugins are loaded', () => {
		expect(reactRules.plugins).toEqual(['jest', 'prettier', 'react', 'babel']);
	});

	test('the correct globals are present', () => {
		expect(Object.keys(reactRules.env)).toEqual(['node', 'es6', 'browser']);
	});

	test('your run-of-the-mill component passes', async () => {
		const result = await lint(
			wrapComponent(`	/* eslint no-empty-function: 0, class-methods-use-this: 0 */
	componentDidMount() {}
	someMethod() {}
	anotherMethod() {}
	render() {
		return <div />;
	}`)
		);

		expect(result.warningCount).toEqual(0);
		expect(result.messages).toHaveLength(0);
		expect(result.errorCount).toEqual(0);
	});

	test('`this` can be used in fat arrow functions.', async () => {
		const result = await lint(
			wrapComponent(`	/* eslint no-unused-vars: 0 */
	someMethod = () => {
		this.want = 'want';
	};

	render() {
		const need = this.want;
		return <div />;
	}`)
		);

		expect(result.warningCount).toEqual(0);
		expect(result.messages).toHaveLength(0);
		expect(result.errorCount).toEqual(0);
	});

	test('browser globals are not undefined.', async () => {
		const result = await lint(
			wrapComponent(`	/* eslint no-console: 0, class-methods-use-this: 0 */
	someMethod = () => {
		document.addEventListener('mouseover', this.handleMouseOver);
		const TWELVE = 12;
		this.add = window.scrollY + TWELVE;
		console.log(this.add);
	};

	handleMouseOver = (event) => {
		console.log("You're hovering");
		console.log(window.scrollY + event.target.name);
	};

	render() {
		return <div key={this.add} />;
	}`)
		);

		expect(result.warningCount).toEqual(0);
		expect(result.messages).toHaveLength(0);
		expect(result.errorCount).toEqual(0);
	});
});

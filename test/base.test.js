const fs = require('fs');
const path = require('path');

const base = require('..');

const files = {base};

fs.readdirSync(path.join(__dirname, '../rules')).forEach((name) => {
	// add all non-react rulesets excluding the base ruleset
	if (name === 'react.js' || name === 'base.js') {
		return;
	}

	files[name] = require(`../rules/${name}`); // eslint-disable-line global-require
});

Object.keys(files).forEach((name) => {
	const config = files[name];

	describe(`Ruleset ${name}`, () => {
		test('does not reference React plugin', () => {
			const hasReactPlugin =
				Object.prototype.hasOwnProperty.call(config, 'plugins') &&
				config.plugins.indexOf('react') !== -1;
			expect(hasReactPlugin).not.toBeTruthy();
		});

		test('has no react/* rules', () => {
			const reactRuleIds = Object.keys(config.rules).filter(
				(ruleId) => ruleId.indexOf('react/') === 0
			);
			expect(reactRuleIds).toHaveLength(0);
		});

		test('has no babel/* rules', () => {
			const babelRuleIds = Object.keys(config.rules).filter(
				(ruleId) => ruleId.indexOf('babel/') === 0
			);
			expect(babelRuleIds).toHaveLength(0);
		});
	});
});

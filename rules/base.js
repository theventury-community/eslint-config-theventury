module.exports = {
	env: {
		node: true,
		es6: true,
	},
	parser: '@babel/eslint-parser',
	parserOptions: {
		ecmaVersion: 8,
		sourceType: 'module',
		ecmaFeatures: {
			jsx: true,
		},
		requireConfigFile: false,
		babelOptions: {
			plugins: [
				'@babel/plugin-proposal-class-properties',
				'@babel/plugin-syntax-class-properties',
				'@babel/plugin-syntax-jsx',
			],
		},
	},
	plugins: ['jest', 'prettier'],
	extends: ['eslint:recommended', 'plugin:jest/recommended', 'prettier'],
	globals: {
		Promise: false,
		JSON: false,
	},
	rules: {
		'prettier/prettier': [
			1,
			{
				singleQuote: true,
				trailingComma: 'es5',
				printWidth: 100,
				tabWidth: 4,
				useTabs: true,
				bracketSpacing: false,
			},
		],
		'no-unused-vars': [
			'error',
			{vars: 'all', args: 'none', ignoreRestSiblings: true, varsIgnorePattern: '^_'},
		],
		'no-await-in-loop': 2,
		'no-console': 2,
		'class-methods-use-this': 1,
		complexity: [1, {max: 30}],
		'consistent-return': 1,
		'default-case': 1,
		'dot-location': 0,
		eqeqeq: 1,
		'max-classes-per-file': [2, 3],
		'no-else-return': 1,
		'no-empty-function': 1,
		'no-eq-null': 1,
		'no-eval': 2,
		'no-invalid-this': 1,
		'no-magic-numbers': [
			1,
			{
				ignore: [-1, 0, 1, 2],
				detectObjects: true,
				ignoreArrayIndexes: true,
			},
		],
		'no-new': 2,
		'no-param-reassign': 2,
		'no-return-assign': 1,
		'no-return-await': 1,
		'no-throw-literal': 2,
		'no-unused-expressions': 1,
		'no-useless-return': 1,
		'require-await': 1,
		yoda: 1,
		'no-shadow': 1,
		'no-undefined': 2,
		'global-require': 2,
		'no-mixed-requires': 1,
		'no-path-concat': 1,
		'no-process-env': 2,
		'no-sync': [
			1,
			{
				allowAtRootLevel: true,
			},
		],
		camelcase: [0, {ignoreDestructuring: true, properties: 'always'}],
		'func-style': 1,
		'id-length': [
			1,
			{
				min: 2,
				exceptions: ['a', 'b', 'i'],
			},
		],
		indent: [
			0,
			'tab',
			{
				SwitchCase: 1,
			},
		],
		'max-depth': [1, 5],
		'max-lines': [
			1,
			{
				max: 800,
				skipBlankLines: true,
				skipComments: true,
			},
		],
		'max-lines-per-function': [
			1,
			{
				max: 200,
				skipBlankLines: true,
				skipComments: true,
			},
		],
		'max-params': [1, 4],
		'no-useless-constructor': 1,
		'prefer-spread': 1,
		// jest rules
		'jest/no-disabled-tests': 1,
		'jest/no-focused-tests': 2,
		'jest/no-identical-title': 2,
		'jest/prefer-to-have-length': 1,
		'jest/valid-expect': 2,
	},
	overrides: [
		{
			files: ['**/test/**/*.js'],
			rules: {
				'no-magic-numbers': 0,
				'no-console': 0,
				'no-invalid-this': 0,
				'no-new': 0,
				'array-callback-return': 0,
				'max-lines-per-function': 0,
			},
		},
		{
			files: ['**/Config.js', '**/config.js'],
			rules: {
				'no-magic-numbers': 0,
				'no-process-env': 0,
			},
		},
	],
};

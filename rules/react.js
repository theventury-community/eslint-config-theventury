const {
	rules,
	env,
	parser,
	parserOptions,
	extends: ext,
	globals,
	plugins,
	overrides,
} = require('./base');

module.exports = {
	env: Object.assign(env, {
		browser: true,
	}),
	parser,
	parserOptions,
	globals,
	plugins: [...plugins, 'react', 'babel'],
	extends: [...ext, 'plugin:react/recommended'],
	rules: Object.assign(rules, {
		'no-invalid-this': 0,
		'babel/no-invalid-this': 1,
		'max-lines-per-function': [
			1,
			{
				max: 800,
				skipBlankLines: true,
				skipComments: true,
			},
		],
	}),
	overrides,
	settings: {
		react: {
			version: 'detect',
		},
	},
};

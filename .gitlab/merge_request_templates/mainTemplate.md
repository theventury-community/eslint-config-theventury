# Goal[^goal]

[^goal]: Provide ticket number and **DoD** of the main **Jira ticket** here. This is the ticket the **Reviewer** will be asked to book on; this should be obvious. If this **MR** closes *multiple* tickets also put their respective **DoD** here and make sure they’re listed in the **Covered Tickets** section below.

# Summary[^summary]

[^summary]: Summarize what this **MR** does to fulfill the **DoD**. Don’t just paste the **Jira ticket** description. This can be a short paragraph or a list of effective changes.

# Covered Tickets[^covered]

[^covered]: **Optional!** If this **MR** closes *multiple* tickets, please reference each here in a list. Make sure that their respective **DoD** can be found in the **Goal** section. Feel free to delete this section if not needed.

# Linked Merge Requests[^linked]

[^linked]: **Optional!** If there are linked **MR**s (because this is a frontend ticket with a corresponding backend ticket and vice versa), please note this here. Note that **both** **MR**s need a full description. Feel free to delete this section if not needed.

# Implementation Notes[^details]

[^details]: **Optional!** Necessary if your **MR** implements a solution that is (1) highly unconventional (2) deviates from defined standards, (3) deviates from what the **Jira ticket’s** summary suggests. In any such case this discrepancy *must* also be documented in your code. Check out our [readable code standards](https://wiki.theventury.com/bin/Flows-Standards/IT/Code-Standards/Readable%20Code) for more information. Feel free to delete this section if not needed.

# Test Scenarios[^scenarios]

[^scenarios]: Copy these from the **Jira ticket** description so they can be referenced by the **Reviewer** from the **MR** for convenience. If no test scenarios exist, provide information as to how the **Reviewer** can test and ensure the **DoD** was met in lieu of that.

# Acceptance Criteria[^criteria]

[^criteria]: Please only **check** the items that relate to your role (**Author** *or* **Reviewer**).

## Author[^author]

The **Author** ensures that:

-   [ ] the **MR** is properly described, meaning that
    -   [ ] it provides the **DoD** for all covered **Jira tickets** and how they were fulfilled
    -   [ ] all covered **Jira tickets** are linked
    -   [ ] linked **Merge Requests** are referenced
    -   [ ] implementation notes were given (where applicable)
    -   [ ] testing instructions are provided
-   [ ] the code is [formatted and linted](https://wiki.theventury.com/bin/Flows-Standards/IT/Code-Standards/Formatting-Linting/)
-   [ ] [documentation](https://wiki.theventury.com/bin/Flows-Standards/IT/Code-Standards/Documentation/) was written (or updated)
-   [ ] [automatic tests](https://wiki.theventury.com/bin/Flows-Standards/IT/Code-Standards/Testing/) were written (or updated)

[^author]: The **Author** is the author and owner of the code; outside of implementing the code, they fill in this template and assign the **Reviewer**.

## Reviewer[^rev]

The **Reviewer** ensures that:

-   [ ] implementation fulfills **DoD** of covered **Jira ticket(s)** and reflects a sound interpretation
-   [ ] code meets [readable code](https://wiki.theventury.com/bin/Flows-Standards/IT/Code-Standards/Readable%20Code) standards
-   [ ] pipeline passes for
    -   [ ] `build` stage and
    -   [ ] `test` stage
-   [ ] code quality check results were taken into account
-   [ ] the test scenarios were either
    -   [ ] covered by automatic tests or
    -   [ ] tested in **Review App** in supported browsers or failing that
    -   [ ] the branch was pulled and tests performed locally
-   [ ] discussions (if raised) were resolved by **Author**

[^rev]: You can find the **Reviewer's** responsibilities [here](https://wiki.theventury.com/bin/Circles/NerdHerd/Development%20Guide/#HWhatdoescodereviewA0entail3F).

***

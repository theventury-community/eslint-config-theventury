# Merge request template

MR templates for TheVentury

# Getting started

For integrating this project we will use `git subtree` the main reason being the files from this repo will be present in the project you are integrating with. Allowing template files to be read and used.

Another upside is that we avoid all the problems that come from using `git submodules`, including a better way of handling conflicts.

For a quick intro to subtree and how to work with it you can read this [blog article](https://www.atlassian.com/git/tutorials/git-subtree) or look at [this repository](https://gitlab.com/theVentury/it-crowd/testprojects/subtree-consumer).

## One time setup

To include this in your project run these commands:

```shell
git remote add mr-templates git@gitlab.com:theVentury/it-crowd/tools/merge-request-template.git
git subtree add --prefix=.gitlab/merge_request_templates mr-templates master  --squash
```

## Quick start

Every developer who wants to make changes in the template must run this command once:
```shell
git remote add mr-templates git@gitlab.com:theVentury/it-crowd/tools/merge-request-template.git --squash
```

Note that if you have done the "one time setup" you may skip the first command.

## Updating the template

In case this repository changes and you want to get the latest version, run:

```shell
git subtree pull --prefix=.gitlab/merge_request_templates mr-templates master --squash
```

Or, in case you want to commit local changes to this repo:

```shell
git subtree push --prefix=.gitlab/merge_request_templates mr-templates feature/name
```

Make sure to change the branch name.

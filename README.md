# eslint-config-theventury

This package provides TheVentury's shared ESLint config.

## Rulesets

### eslint-config-theventury

The default config contains all `eslint/recommended` rules and a selection of Best Practice, Style and ECMAScript6 rules.

Prettier rules are included as well as an override for test folders.

### eslint-config-theventury/react

This config comprises the base ruleset and React-specific rules.

## Usage

1. Configure your package manager to use the GitLab package registry for the `theventury-community` scope.

   ### npm
   Add the following line to you `.npmrc` at package root (or in your `HOME` folder).

   ```conf
   @theventury-community:registry = https://gitlab.com/api/v4/packages/npm/
   ```

   Both `npm` and `yarn` will respect this.

   Be sure to add this file in your `Dockerfile`.

   ```dockerfile
   ADD .npmrc .
   ```

   ### Yarn Modern aka berry
   Add the following to you `.yarnrc.yml` at package root.

   ```conf
   npmScopes:
      theventury-community:
         npmRegistryServer: "https://gitlab.com/api/v4/packages/npm/"
   ```

   `yarn berry` will respect this.

   Be sure to add this file in your `Dockerfile`.

   ```dockerfile
   ADD .yarnrc.yml .
   ```

2. Add the package as a dependency:

   ```sh
   npm i @theventury-community/eslint-config-theventury --save-dev # or
   yarn add @theventury-community/eslint-config-theventury --dev
   ```

3. Add a `.eslintrc` config at the root of your project (or package) containing the following:

   ```json
   {
       "extends": "@theventury-community/theventury"
   }
   ```

  If you need a specific ruleset (currently only one for React exists), extend as you normally would:

   ```json
   {
       "extends": "@theventury-community/theventury/react"
   }
   ```

  Note that you need to prefix the config name by the scope for this to work.

4. Adapt to your liking with more rules, overrides and plugins.

## Contribute

Feel free to contribute.

Note that this repository uses [husky](https://www.npmjs.com/package/husky).

Commits will be prettified and linted or fail. Pushes will be tested or fail.
